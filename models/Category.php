<?php
/**
 * 分類規則資訊表
 */

namespace Crawler\Models;

class Category extends \Phalcon\Mvc\Model
{

    /**
     * 主鍵
     * @var integer
     */
    public $categoryId;

    /**
     * 類型
     * @var integer
     */
    public $type;

    /**
     * 分類名稱
     * @var string
     */
    public $label;

    /**
     * 排序值
     * @var integer
     */
    public $weight;

    /**
     * 關連帳號編號
     * @var integer
     */
    public $PeopleId;


    public function initialize()
    {
        $this->hasMany('categoryId', "Crawler\Models\CategoryTabs", "CategoryId", array('alias' => 'categorytabs'));
        $this->hasManyToMany(
            "categoryId",
            "Crawler\Models\CategoryTabs",
            "CategoryId", "TabsId",
            "Crawler\Models\Tabs",
            "tabsId",
            array('alias' => 'tabses')
        );
        $this->belongsTo("PeopleId", "Crawler\Models\People", "peopleId", array('alias' => 'relpeople'));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'categoryId' => 'categoryId',
            'type' => 'type',
            'label' => 'label',
            'weight' => 'weight',
            'PeopleId'=> 'PeopleId'
        );
    }

}
