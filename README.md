# pkrobot爬蟲機器技術說明文件

---
## 未來任務進度
- [ ] 任務項目[標記寫法](https://github.com/blog/1375%0A-task-lists-in-gfm-issues-pulls-comments)。

- *擴充功能*

#### 使用方法

---
## 版本演進
v1.3(2017/04/10~16)
    -

v1.2(2016/11/23)
    - 修正pttcrawl規則
        + 增加持續追蹤文章紀錄表。

1.1dev(02/19~21)
    - crawl部分
        + 修正使用facepy抓取發生例外時的檢查機制
        + 新增寫入facebook額外資訊欄位[紀錄類型]並調整以連結、標題[message]判斷重複內容
    - 操作介面
        + 修改使用者所屬爬蟲紀錄對應變數從tabs改為tablist
        + 修改建立爬蟲方式，不重複建立相同來源爬蟲可用設定增加爬蟲紀錄
        + 修改建立分類判斷爬蟲類型方式

1.0dev(02/17~02/18)
    - 修正判斷空內容作法，改以sourcelink原文連結否則以標題作為判斷依據
    - 修正拆解dirname或是網址內對應post或profile參數內容
    - 隱藏系統參數設定介面（目前不需要）
    - 修正分類群組新增/刪除後可以即時更新session資訊

0.9dev
    - 完成套用yadcf與atatable 後端php filter處理功能。

0.8dev
    - 目前抓取資料python端設定每1小時執行一次。
    - 完成php介面參數設定與使用流程，確認pythond也可以正常讀取json檔案。
    - php介面頁籤設定功能並可以設定後動態產生新的頁籤。
    - 可支援刪除爬蟲或是爬蟲群組。
    - 可動態針對特定爬蟲群組進行數據更新而非單次全部處理。
    - facebook暫時排除影片檔解析、PTT可過濾總回應數量超過30則才紀錄。

0.6.3dev
    - 處理Python FB parser流程優化，包含可設定只抓取多久內、可終止執行(關閉broswer)、可同步多粉絲頁面處理
    - 儲存html檔後在透過bs4進行解析

0.6.1dev
    - 調整PTT版塊區塊可切換八卦版、笨版、職場版列表功能。
    - 發佈資訊可以將內文使用tooltips顯示
    - 人氣資訊過濾推文人數超過30人(python處理)
    - 預設只載入發佈時間48小時內文章，可加功能顯示更多7天內文章
