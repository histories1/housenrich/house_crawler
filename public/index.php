<?php

use Phalcon\Mvc\Micro;

error_reporting(E_ALL);
ini_set('display_errors', 1);

// define global const
define('PPS_APP_PROJECTNAME', 'crawler');
define('PPS_APP_APPSPATH', realpath('..'));

try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../config/config.php";

    /**
     * Include Services
     */
    include PPS_APP_APPSPATH . '/config/services.php';

    /**
     * Include Autoloader
     */
    include PPS_APP_APPSPATH . '/config/loader.php';

    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new Micro($di);

    // Executed before every route is executed
    // Return false cancels the route execution
    $app->before(function () use ($app) {
        if ($app['session']->has('auth-identity') == false &&
            $app['router']->getRewriteUri() != '/login' ) {

            $app['flashSession']->error("尚未登入帳號，請登入後使用。");
            $app['response']->redirect("/login")->sendHeaders();
            // Return false stops the normal execution
            return false;
        }
        return true;
    });

    /**
     * Incude Application
     */
    include PPS_APP_APPSPATH . '/app.php';

    /**
     * Handle the request
     */
    $app->handle();

} catch (\Exception $e) {
    echo $e->getMessage();
}
