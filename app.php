<?php
/**
 * 處理所有crawldata_591對應crawldata內再處理比對作業
 * */
$app->get('/filter', function () use ($app) {
    // 每200筆一次處理方式，將crawldata_591部份取出進行比對。
    $limit=200;
    $offset=0;
    do{
        $cdata591=\Crawler\Models\Crawldata591::find(["limit"=>$limit,"offset"=>$offset]);
        echo "目前處理範圍:".($offset*$limit).'~'.($offset*$limit+$limit).'<BR/>';
        // addressSegment轉處理
        foreach($cdata591 as $rawdata){
            // 處理地址顯示
            preg_match("/(.+[路|段|道|街|村|巷]).*/u", $rawdata->addressSegment, $m);
            $mapping_addr=$m[1];
            // 搜尋地址
            $rows=\Crawler\Models\Crawldata::find(["addressSegment LIKE '{$mapping_addr}%'"]);
            $amt=count($rows);
            if( $amt >= 2 ){
                echo "取出{$amt}比對數據:[所在樓層]{$rawdata->floor_at}、[坪數]{$rawdata->area}、[屋齡]{$rawdata->houseage}<BR/>";
                // 範例資料
                $d1=$rows[0]->toArray();
                $d2=$rows[1]->toArray();
                echo '<pre>';
                var_dump(['title'=>$d1['title'], 'address'=>$d1['addressSegment'],'floor_at'=>$d1['floorAt'],'area'=>$d1['areaTotal'],'houseage'=>$d1['houseage']]);
                echo '</pre>';
                echo '<pre>';
                var_dump(['title'=>$d2['title'], 'address'=>$d1['addressSegment'],'floor_at'=>$d2['floorAt'],'area'=>$d2['areaTotal'],'houseage'=>$d2['houseage']]);
                echo '</pre>';
            echo '<BR/>';


            // 比對相符時，取得低總價欄位，寫入crawldata欄位新增降價價格「priceDown」(接受Null)內！
            }
        }
        break;
        echo '<BR/>';
        $offset++;
    }while( count($cdata591)>0 );
});


/**
 * Add your routes here
 * @TODO need DI:request / response / flashSession /
 */
$app->get('/', function () use ($app) {
    // 處理登出
    if( isset($_GET['logout']) ){
        $app['session']->remove('auth-identity');
        $app->response->redirect("/")->sendHeaders();
    }

    if( empty($this->session->get('auth-identity')->tablist) ){
        $tablists = \Crawler\Models\Tabs::find();
    }else{
        $tablists = $this->session->get('auth-identity')->tablist;
    }

    $MetaData= new \Phalcon\Mvc\Model\MetaData\Memory();
    $tabsArr=array();
    foreach ($tablists as $tab) {
        $tdata = $tab->toArray();
        $modelName = "\\Crawler\\Models\\Crawldata".ucfirst($tab->type);
        $tdata['fields'] = $MetaData->getNonPrimaryKeyAttributes( new $modelName() );
        $tdata['typeLabel'] = $tab->getTypeLabel();

        $tabsArr[] = $tdata;
    }
    $app['view']->tabs = $tabsArr;
    $app['view']->session = $app->session->get('auth-identity');
    echo $app['view']->render('layout2');
});

function resetSession(&$app, $user=null ){
    if( !$user ){
        $user=\Crawler\Models\People::findFirst( $app->session->get('auth-identity')->peopleId );
    }
    $udata=$user->toArray();
    unset($udata['password']);
    // 處理附加資訊
    $userCategories=\Crawler\Models\Category::findByPeopleId( $udata['peopleId'] );
    $udata['categories'] = $userCategories->toArray();
    if( $app->session->has('auth-identity') ){
        $app->session->remove('auth-identity');
    }
    $app->session->set('auth-identity', (object)$udata);
}

$app->map('/test', function () use ($app) {

    var_dump($app->session->get('auth-identity'));
    // phpinfo();
    // $str = 'ms1999';
    // echo $app->security->hash($str);die();
    // $str='https://www.facebook.com/profile.php?id=100000944336615&fref=ts';
    // if( preg_match("/(http[s]?:\/\/)?www\.facebook\.com\/([\w|\.|\?|=]+).*/", $str, $match) ){
    //     var_dump($match);
    //     if( !empty($match[2]) ){
    //         $tab->dirname = $match[2];
    //     }
    // }elseif( preg_match("/(http[s]?:\/\/)?www\.ptt\.cc\/bbs\/(\w+).*/", $str, $match) ){
    //     if( !empty($match[2]) ){
    //         $tab->dirname = $match[2];
    //     }
    // }
})->via(array('GET', 'POST'));

/**
 * 處理刪除爬蟲追蹤源
 */
$app->get('/deltab/{tabsId}', function ($tabsId) use ($app) {
    $tabs=\Crawler\Models\Tabs::findFirst( $tabsId );
    if( !empty($tabs) ){

        $sessionIds = unserialize($app->session->get('auth-identity')->tablist);
        foreach($sessionIds as $i => $id) {
            if( $tabsId == $id){
                unset($sessionIds[$i]);
                $people = \Crawler\Models\People::findFirst($app->session->get('auth-identity')->peopleId);
                $people->tablist = serialize($sessionIds);
                if( $people->save() ){
                    $app->session->get('auth-identity')->tablist = serialize($sessionIds);
                }
                break;
            }
        }

    }
    $app->response->redirect("/")->sendHeaders();
});
/**
 * 處理刪除爬蟲群組
 */
$app->get('/delcate/{categoryId}', function ($categoryId) use ($app) {
    $category=\Crawler\Models\Category::findFirst( $categoryId );
    // var_dump($category->toArray());die();
    if( !empty($category) ){
        $app['flashSession']->success('已成功移除'.$category->label.'群組');
        $category->delete();
        resetSession($app);
    }
    return $app->response->redirect('/');
    // $app->response->redirect("/")->sendHeaders();
});


/**
 * 處理擷取json資料到db
 */
$app->get('/json-to-mysql/{typelabel}/{categoryId}', function ($typelabel, $categoryId) use ($app) {
    require_once 'crawldata.php';

    $crawl = new crawldata( $typelabel , $categoryId);
    // logger or nothing
    if( !$crawl->save() ){
        echo json_encode(array('result'=>'fail', 'msg'=>'處理更新寫入新的爬蟲紀錄發生錯誤！'));
    }else{
        echo json_encode(array('result'=>'done', 'msg'=>'完成匯入爬蟲紀錄，將更新列表資訊！'));
    }
});


$app->post('/ajax-load', function () use ($app) {
    echo json_encode(array('msg'=>'load success!'));
});

/**
 * Ajax Load Facebook Crawl data
 */
$app->map('/loadcrawl', function () use ($app) {

    // 處理群組項目
    $tabNames=array();
    // if( $_POST['categoryId'] != 'all' ){
    //     $tabs = \Crawler\Models\CategoryTabs::findByCategoryId($_POST['categoryId']);
    //     foreach ($tabs as $record) {
    //         $tabNames[] = $record->reltabs->dirname;
    //     }
    // }else{
    //     if( !empty($app->session->get('auth-identity')->tablist) ){
    //         $tabsIds = implode(",", unserialize($app->session->get('auth-identity')->tablist));
    //         if( $_POST['type'] == 'FB'){
    //             $tabs=\Crawler\Models\Tabs::find("type<>".\Crawler\Models\Tabs::TYPE_PTT." AND tabsId IN ({$tabsIds})" );
    //         }elseif( $_POST['type'] == 'PTT'){
    //             $tabs=\Crawler\Models\Tabs::find("type=".\Crawler\Models\Tabs::TYPE_PTT." AND tabsId IN ({$tabsIds})" );
    //         }
    //         foreach ($tabs as $tab) {
    //             $tabNames[] = $tab->dirname;
    //         }
    //     }else{
    //         $tabNames=array();
    //     }
    // }
    $tabNames =['591','sinyi'];

    $phql = $app['modelsManager']->createBuilder()
                                ->columns(['c.crawldataId','c.title', 'c.coverpath','c.singlePrice', 'c.sourcelink','c.addressSegment','c.usefor','t.label','t.source'])
                                ->from(array('c'=>'\Crawler\Models\Crawldata'))
                                ->leftJoin('\Crawler\Models\TabsCrawldata', 'c.crawldataId=tc.CrawldataId', 'tc')
                                ->leftJoin('\Crawler\Models\Tabs', 't.dirname=tc.Dirname', 't')
                                ->inWhere('t.dirname', $tabNames)
                                ->groupBy('c.crawldataId');
    $default = $phql->getQuery()->execute();

    // 是否有過濾處理
    $withoutfilter = true;
    if( $_POST['type'] == '591' ){

        /**
         * 處理各類過濾參數
         */
        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                $withoutfilter = false;
                switch ($param['data']) {
                    case 'source':
                    $phql->andWhere("t.label='{$param['search']['value']}'");
                    break;
                    case 'time':
                    if( ($timestamp=strtotime($param['search']['value'])) !== false ){
                        $phql->andWhere("c.initTime>={$timestamp}");
                    }
                    break;
                    case 'title':
                    $phql->andWhere("c.title LIKE '%{$param['search']['value']}%' OR c.content LIKE '%{$param['search']['value']}%'");
                    break;
                }
            }
        }
        // die();

        $columns = array('t.label', 'c.initTime' );
        if( isset($_POST['order'][0]['column']) ){
            $phql->orderBy($columns[$_POST['order'][0]['column']].' '.$_POST['order'][0]['dir']);
        }else{
            $phql->orderBy("c.initTime DESC");
        }

        $phql->limit($_POST['length']);
        if( $_POST['start'] != 0 ){
            $phql->offset($_POST['start']);
        }
        $rows = $phql->getQuery()->execute();


        foreach($rows as $crawl){
            $data=$crawl->toArray();



            $lists[] = $data;
        }
    }elseif( $_POST['type'] == 'PTT'){

        foreach ($_POST['columns'] as $index => $param) {
            if( $param['searchable'] == 'true' && !empty($param['search']['value']) ){
                $withoutfilter = false;
                switch ($param['data']) {
                    case 'source':
                    $phql->andWhere("t.label='{$param['search']['value']}'");
                    break;
                    case 'time':
                    if( ($timestamp=strtotime($param['search']['value'])) !== false ){
                        $phql->andWhere("c.initTime>={$timestamp}");
                    }
                    break;
                    case 'title':
                    $phql->andWhere("c.title LIKE '%{$param['search']['value']}%'");
                    break;
                }
            }
        }

        $columns = array('t.label', 'c.initTime' );
        if( isset($_POST['order'][0]['column']) ){
            $phql->orderBy($columns[$_POST['order'][0]['column']].' '.$_POST['order'][0]['dir']);
        }else{
            $phql->orderBy("c.initTime DESC");
        }

        $phql->limit($_POST['length']);
        if( $_POST['start'] != 0 ){
            $phql->offset($_POST['start']);
        }
        $rows = $phql->getQuery()->execute();

        foreach($rows as $pttcrawl){
            $ai=0;$ni=0;$gi=0;$bi=0;
            $row = \Crawler\Models\Crawldata::findFirst($pttcrawl->crawldataId);
            $all=$row->getextendinfo("key='總數'")->getFirst();
            if( !empty($all) ){
                $ai = $all->value;
            }
            $normal=$row->getextendinfo("key='一般'")->getFirst();
            if( !empty($all) ){
                $ni = $normal->value;
            }
            $good=$row->getextendinfo("key='推文'")->getFirst();
            if( !empty($all) ){
                $gi = $good->value;
            }
            $bad=$row->getextendinfo("key='噓文'")->getFirst();
            if( !empty($all) ){
                $bi = $bad->value;
            }

            $data['source'] = "<a target='_blank' href='https://www.ptt.cc/bbs/{$pttcrawl->source}/index.html'>{$pttcrawl->label}</a>";
            $data['title'] = "<a target='_blank' href='{$pttcrawl->sourcelink}'>{$pttcrawl->title}</a>";
            $data['time'] = date('m月d日 H時i分s秒',$pttcrawl->initTime);
            $data['content'] = mb_strcut(strip_tags($pttcrawl->content), 0, 90, "UTF-8");
            $data['content'].= '<button class="btn btn-xs btn-info" title="'.strip_tags($pttcrawl->content).'" data-toggle="tooltip" data-placement="top">詳細</button>';
            $data['social'] = "<button class='btn btn-xs btn-success' title='總回文{$ai}/一般{$ni}/噓文{$bi}' data-toggle='tooltip' data-placement='right'>
                               <b>{$gi}</b>推</button>";
            // print("
            // <tr class='fb-{$fbcrawl->tabses->getFirst()->tabsId}'>
            // </tr>
            // ");
            $lists[] = $data;
        }
    }

    // 組合JSON格式回傳
    if( !empty($lists) ){
        if( $withoutfilter ){
            $Filtered = count($default);
        }else{
            $Filtered = count($lists);
        }
        $JSON = array('draw' => $_REQUEST['draw'],
                      'recordsTotal' => count($default),
                      'recordsFiltered' => $Filtered,
                      'data' => $lists );
    }else{
        $JSON = array('draw' => $_REQUEST['draw'],
                      'recordsTotal' => count($default),
                      'recordsFiltered' => 0,
                      'data' => array() );
    }
    echo json_encode($JSON);

})->via(array('GET', 'POST'));


/**
 * Ajax Load Configure Phase
 */
$app->map('/phaseConf', function () use ($app) {
    $form = new Crawler\Forms\FormBuild(new \stdClass, array(
                                'target_dir'=> PPS_APP_APPSPATH . '/config/configure/',
                                'belongto' => 'configureForm',
                                'sendbtnname' => '更新設定'
                              ));
    /* 處理系統參數設定 */
    if( $app->request->isPost()  ){
        // set $form as FormBuild
        if( !$form->isValid($_POST) ){
            foreach ($form->getMessages() as $message) {
                $app->flashSession->warning($message);
            }
            return $app->response->redirect('/phaseConf');
        }
        $configfilepath = realpath(PPS_APP_APPSPATH . '/config/').'/application.json';
        if( file_exists($configfilepath) ){
            unlink($configfilepath);
        }
        //act4. 使用 file_put_contents 產生檔案到apps/config！
        //file_put_contents($configfilepath, json_encode($app->request->getPost()) );
        $file = fopen($configfilepath,"w");
        $string = str_replace("{","{\n\t",json_encode($app->request->getPost()));
        $string = str_replace("\",\"","\",\n\t\"",$string);
        $string = str_replace("}","\n}",$string);
        fputs($file,$string);
        fclose($file);
        $app['flashSession']->success('已更新參數設定值！');
        //return $app->response->redirect('phaseConf');
    }
    $app['view']->form = $form;
    echo $app['view']->render('phaseconf');
})->via(array('GET', 'POST'));

function resetUserTabs(&$app, $tabsId=null){
    $people=\Crawler\Models\People::findFirst($app['session']->get('auth-identity')->peopleId);
    if( !empty($tabsId) ){
        $_POST['tabsId'][] = $tabsId;
        $people->tablist=serialize($_POST['tabsId']);
        if( $people->save() ){
            $app['session']->get('auth-identity')->tablist = $people->tablist;
        }
    }elseif( !empty($_POST['tabsId']) and serialize($_POST['tabsId']) != $app['session']->get('auth-identity')->tablist ){
        $people->tablist=serialize($_POST['tabsId']);
        if( $people->save() ){
            $app['session']->get('auth-identity')->tablist = $people->tablist;
        }
    }elseif( empty($_POST['tabsId']) and !empty($app['session']->get('auth-identity')->tablist) ){
        $people->tablist=null;
        if( $people->save() ){
            $app['session']->get('auth-identity')->tablist = null;
        }
    }
}

/**
 * 新增一塊儀表板作法
 */
$app->map('/create-tabs', function () use ($app) {
    $tab = new \Crawler\Models\Tabs();
    if( $app->request->isPost()  ){
        // 新增爬蟲
        if( isset($_POST['type']) and !empty($_POST['label']) and !empty($_POST['source']) ){

            // if( preg_match("/(http[s]?:\/\/)?www\.facebook\.com\/([\w|\.|\?|=]+).*/", $_POST['source'], $match) ){
            //     if( !empty($match[2]) ){
            //         $dirname = $match[2];
            //     }
            // }elseif( preg_match("/(http[s]?:\/\/)?www\.ptt\.cc\/bbs\/(\w+).*/", $_POST['source'], $match) ){
            //     if( !empty($match[2]) ){
            //         $dirname = $match[2];
            //     }
            // // 其他類型（591 / 信義）
            // }else{

            // }
            // 檢查dirname項目是否為一
            $exist = \Crawler\Models\Tabs::findFirst(["label=:l: AND source=:s:",'bind'=>['l'=>$_POST['label'],'s'=>$_POST['source']]]);
            if( $exist ){
                $app['flashSession']->warning("{$_POST['label']}已建立過請直接從列表設定");
            }else{
                $tab->type = $_POST['type'];
                $tab->label = $_POST['label'];
                $tab->source = $_POST['source'];
                $tab->endstring = $_POST['endstring'];
                // 加入配置帳號
                $tab->PeopleId = $app['session']->get('auth-identity')->peopleId;
                $tab->setDirname();
                $tab->crontime = $_POST['condition']['crontime'];
                $tab->crawllimit = $_POST['condition']['crawllimit'];

                if( !$tab->save() ){
                    $app['flashSession']->error('建立爬蟲資訊發生錯誤，訊息：'. implode(', ',$tab->getMessages()));
                }else{
                    // 加入session內
                    resetUserTabs($app, $tab->tabsId);

                    $app['flashSession']->success('已建立新爬蟲目標！');
                }
            }

        // 處理配置使用者爬蟲項目
        }else{
            resetUserTabs($app);
        }

        return $app->response->redirect('');
    }
    $app['view']->tabs = $tab;
    echo $app['view']->render('create-tabs');
})->via(array('GET', 'POST'));

/**
 * 帳號變更密碼
 */
$app->map('/change-password', function () use ($app) {
    if( $app->request->isPost()  ){
        $user = \Crawler\Models\People::findFirst( $app['session']->get('auth-identity')->peopleId );
        if( $app->security->checkHash($_POST['oldpasswd'], $user->password) ){
            $user->password = $app->security->hash($_POST['password']);
            $app['flashSession']->success('您的密碼已變更完成，請使用新密碼重新登入');
            $app['session']->remove('auth-identity');
            return $app->response->redirect('');
            // $app->response->redirect('')->sendHeaders();
        }else{
            $app['flashSession']->error('您輸入的舊密碼錯誤，請重新處理');
            // $app->response->redirect('')->sendHeaders();
            return $app->response->redirect('');
        }
    }
    echo $app['view']->render('change-password');
})->via(array('GET', 'POST'));

/**
 * 新增一塊儀表板作法
 */
$app->map('/create-category/{type}', function ($type) use ($app) {
    if( $app->request->isPost()  ){
        $category = new \Crawler\Models\Category();
        $category->type = $type;
        $category->label = $_POST['label'];
        // 加入配置帳號
        $category->PeopleId = $app['session']->get('auth-identity')->peopleId;

        foreach($_POST['TabsId'] as $i => $tabid ){
            $ct[$i] = new \Crawler\Models\CategoryTabs();
            $ct[$i]->TabsId = $tabid;
        }
        $category->categorytabs = $ct;

        if( !$category->save() ){
            $app['flashSession']->error('建立爬蟲群組發生錯誤，訊息：'. implode(', ',$category->getMessages()));
        }else{
            resetSession($app);
            $app['flashSession']->success('已建立新爬蟲群組！');
        }
        return $app->response->redirect('');
    }
    //建立分類類型
    $app['view']->type = $type;
    $app['view']->session = $app->session->get('auth-identity');
    echo $app['view']->render('create-category');
})->via(array('GET', 'POST'));

/**
 * Phase for login
 */
$app->map('/login', function () use ($app) {
    $form = new \Phalcon\Forms\Form();
    $account = new \Phalcon\Forms\Element\Text('account', array(
                        'placeholder' => '輸入帳號',
                        'class'       => 'form-control'
                    ));
    $account->addValidator( new \Phalcon\Validation\Validator\PresenceOf(array('account' => '請輸入帳號欄位')) );
    $form->add($account);
    $password = new \Phalcon\Forms\Element\Text('password', array(
                        'placeholder' => '輸入密碼',
                        'class'       => 'form-control'
                    ));
    $password->addValidator( new \Phalcon\Validation\Validator\PresenceOf(array('message' => '請輸入密碼欄位')) );
    $form->add($password);

    if( $app->request->isPost()  ){

        if( $app->session->get('auth-identity') ){
            // $app->response->redirect('')->sendHeaders();
            return $app->response->redirect('/');
        }else{
            try {
                if ($form->isValid($app->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $app['flashSession']->error($message->getMessage());
                    }
                } else {

                    $user=\Crawler\Models\People::findFirstByAccount( strtolower($_POST['account']) );
                    if($user == false) {
                        $app['flashSession']->warning( '帳號錯誤或不存在，請重新輸入！' );
                        return $app->response->redirect('/login');
                    }elseif( !$app->security->checkHash($_POST['password'], $user->password) ){
                        $app->flashSession->warning( '您的密碼錯誤，請重新輸入！' );
                        return $app->response->redirect('/login');
                    }else{
                        resetSession($app, $user);
                        $user->lastTime = time();
                        $user->save();
                        $app['flashSession']->success( '您已成功登入' );
                        // $app->response->redirect('')->sendHeaders();
                        return $app->response->redirect('/');
                    }
                }
            } catch (AuthException $e) {
                $app['flashSession']->error($e->getMessage());
            }
        }
    }

    echo $app['view']->render('login');
})->via(array('GET', 'POST'));

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
