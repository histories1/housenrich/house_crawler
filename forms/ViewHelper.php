<?php

namespace Pkrobot\Forms;

use \Phalcon\Tag;

class ViewHelper extends \Personalwork\Mvc\View\Helpers\FormGroups
{

    /**
     * Personalwork\Mvc\View\Helpers\FormWithGroupelm::__construct()
     */
    public function __construct(\Personalwork\Forms\Form $form) {
        $this->form = $form;
    }

    /**
     * 透過 FormWithGroupelm::toHtml 動態呼叫此method
     * 因此需要使用 protected 類型
     */
    protected function _gen_widget() {
        $groupelms = $this->form->getFieldSet();

        foreach ($groupelms as $index => $set) {

            $this->html.= "\t\t".Tag::tagHtml('section',array('class'=>'widget'), FALSE, TRUE, TRUE);
            //widget-header
            $this->html.= "<div class='widget-header'><h3>{$set->label}</h3>";
            //widget-toolbar
            if( !empty($set->append) ){
            $this->html.= "<div class='btn-group widget-header-toolbar'>{$set->append}</div>";
            }
            $this->html.= "</div>";

            //widget-content
            $this->html.= "<div class='widget-content'>";
            $this->generateElements( (array)$set->elements );
            $this->html.= "</div>";

            $this->html.= Tag::tagHtmlClose('section').PHP_EOL;
        }
    }
}
