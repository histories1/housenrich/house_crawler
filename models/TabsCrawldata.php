<?php
/**
 * 爬蟲機器人擷取紀錄對照表
 */

namespace Crawler\Models;

class TabsCrawldata extends \Phalcon\Mvc\Model
{

    /**
     * 主鍵
     * @var integer
     */
    public $tcId;

    /**
     * 關聯爬蟲機器定義編號
     * @var integer
     */
    public $TabsId;

    /**
     * 關聯爬蟲機器紀錄編號
     * @var integer
     */
    public $CrawldataId;

    /**
     * 關聯爬蟲機器識別碼
     * @var string
     */
    public $Dirname;


    public function initialize()
    {
        $this->belongsTo("TabsId", "Crawler\Models\Tabs", "tabsId", array('alias' => 'reltabs'));
        $this->belongsTo("CrawldataId", "Crawler\Models\Crawldata", "crawldataId", array('alias' => 'relcrawldata'));
    }


    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'tcId' => 'tcId',
            'TabsId' => 'TabsId',
            'CrawldataId' => 'CrawldataId',
            'Dirname' => 'Dirname'
        );
    }

}
