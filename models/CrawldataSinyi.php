<?php

namespace Crawler\Models;

class CrawldataSinyi extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $crawldataId;

    /**
     * @Comment("物件標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("城市")
     *
     * @var string
     */
    public $city;

    /**
     * @Comment("行政區")
     *
     * @var string
     */
    public $district;

    /**
     * @Comment("總價")
     *
     * @var double
     */
    public $price_total;

    /**
     * @Comment("降價價格")
     *
     * @var double
     */
    public $price_down;

    /**
     * @Comment("單價")
     *
     * @var double
     */
    public $princ_single;

    /**
     * @Comment("信義物件網址")
     *
     * @var string
     */
    public $url_houseitem;

    /**
     * @Comment("封面圖網址")
     *
     * @var string
     */
    public $cover_src;

    /**
     * @Comment("格局資訊")
     *
     * @var string
     */
    public $patterns;

    /**
     * @Comment("所在樓層")
     *
     * @var string
     */
    public $floor_at;

    /**
     * @Comment("總樓層")
     *
     * @var string
     */
    public $floor_total;

    /**
     * @Comment("屋齡資訊")
     *
     * @var string
     */
    public $houseage;

    /**
     * @Comment("類型")
     *
     * @var string
     */
    public $house_type;

    /**
     * @Comment("用途")
     *
     * @var string
     */
    public $house_usefor;

    /**
     * @Comment("車位資訊")
     *
     * @var string
     */
    public $parking;

    /**
     * @Comment("總坪數")
     *
     * @var string
     */
    public $area;

    /**
     * @Comment("用途")
     *
     * @var string
     */
    public $usefor;

    /**
     * @Comment("熱門程度(電話數、瀏覽數)")
     *
     * @var integer
     */
    public $visited;

    /**
     * @Comment("更新時間點")
     *
     * @var string
     */
    public $updatemoment;

    /**
     * @Comment("地圖網址")
     *
     * @var string
     */
    public $url_housemap;

    /**
     * @Comment("緯度")
     *
     * @var double
     */
    public $lat;

    /**
     * @Comment("經度")
     *
     * @var double
     */
    public $lng;

    /**
     * @Comment("詳細地址(物件內容頁)")
     *
     * @var string
     */
    public $address;

    /**
     * @Comment("社區資訊")
     *
     * @var string
     */
    public $community;

    /**
     * @Comment("主建物坪數")
     *
     * @var double
     */
    public $areaMain;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'crawldata_sinyi';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CrawldataSinyi[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CrawldataSinyi
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
