<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader
// ->registerDirs(array(
//         $config->application->modelsDir,
// ))
->registerNamespaces(array(
        ucfirst(PPS_APP_PROJECTNAME) . '\Models'                  => __DIR__ . '/../models/',
        ucfirst(PPS_APP_PROJECTNAME) . '\Forms'                 => __DIR__ . '/../forms/',
))
->register();

if (!file_exists(PPS_APP_APPSPATH . '/vendor/autoload.php')) {
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}
require_once PPS_APP_APPSPATH . '/vendor/autoload.php';
