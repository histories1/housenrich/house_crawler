<?php

use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\DI\FactoryDefault;
use Phalcon\Security;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Model\Manager as ModelsManager;

$di = new FactoryDefault();

$di['session'] = function () {
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
};

/**
 * Sets the view component
 */
$di['view'] = function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
};

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di['url'] = function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
};

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di['db'] = function () use ($config) {
    return new DbAdapter(array(
        "host" => $config->database->host,
        "username" => $config->database->username,
        "password" => $config->database->password,
        "dbname" => $config->database->dbname,
        "charset" => $config->database->charset,
        "options"   => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
};

$di['modelsManager'] = function () {
    return new ModelsManager();
};

$di->set('security', function () {
    $security = new Security();
    // Set the password hashing factor to 12 rounds
    $security->setWorkFactor(12);
    return $security;
}, true);

$di['flashSession'] = function() {
    return new \Phalcon\Flash\Session(array(
        'error' => 'alert alert-danger text-center',
        'success' => 'alert alert-success text-center',
        'notice' => 'alert alert-info text-center',
        'warning' => 'alert alert-warning text-center',
    ));
};
