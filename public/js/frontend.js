/**
 * 預設首頁底部載入js檔清單
 */
// toggle function
$.fn.clickToggle = function( f1, f2 ) {
    return this.each( function() {
        var clicked = false;
        $(this).bind('click', function() {
            if(clicked) {
                clicked = false;
                return f2.apply(this, arguments);
            }

            clicked = true;
            return f1.apply(this, arguments);
        });
    });
}

$.extend( $.fn.dataTable.defaults, {
    scrollCollapse: true,
    processing: true,
    serverSide: true,
    dom: "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    pageLength: 15,
    language: {
        "emptyTable"    : "目前沒有任何（匹配的）資料。",
        "sProcessing":   "處理中...",
        "sLengthMenu":   "顯示 _MENU_ 項結果",
        "sZeroRecords":  "沒有匹配結果",
        "sInfo":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
        "sInfoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
        "sInfoFiltered": "(從 _MAX_ 項結果過濾)",
        "sInfoPostFix":  "",
        "sSearch":       "搜索:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "首頁",
            "sPrevious": "上頁",
            "sNext":     "下頁",
            "sLast":     "尾頁"
        }
    }
});

$(function() {
	$("[rel=tooltip]").tooltip();

    // widget toggle expand
    $('.widget .btn-toggle-expand').clickToggle(
        function(e) {
            e.preventDefault();
            target=$(this).parents('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-down').toggleClass('fa-chevron-up').end().find('i.fa-chevron-up').toggleClass('fa-chevron-down');
        },
        function(e) {
            e.preventDefault();
            target=$(this).parents('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-up').toggleClass('fa-chevron-down').end().find('i.fa-chevron-down').toggleClass('fa-chevron-up');
        }
    );

    /*****************************
    /*  WIDGET WITH AJAX ENABLE
    /*****************************/
    $('.widget-header-toolbar .btn-ajax').click( function(e){
        e.preventDefault();
        $theButton = $(this);

        // 找出目前頁籤
        elmA=$theButton.parents('.widget-header').find('li.active > a');
        typelabel=$(elmA).data('typelabel');
        categoryId=$(elmA).data('cateid');
        table=$(elmA).data('table');

        $.ajax({
            url: '/json-to-mysql/'+typelabel+'/'+categoryId,
            type: 'GET',
            dataType: 'json',
            cache: false,
            beforeSend: function(){
                $theButton.prop('disabled', true);
                $theButton.find('i').removeClass('fa-refresh').addClass('fa-spinner fa-spin');
                $theButton.find('span').text('載入中...');
            },
            success: function( data, textStatus, XMLHttpRequest ) {
                if( data.result == 'done' ){
                    $(table).DataTable().ajax.reload();
                }
            },
            error: function( XMLHttpRequest, textStatus, errorThrown ) {
                console.log("AJAX ERROR: \n" + errorThrown);
            },
            complete: function(){
                $theButton.prop('disabled', false);
                $theButton.find('i').removeClass('fa-spinner fa-spin').addClass('fa-refresh');
                $theButton.find('span').text('更新');
            }
        });
    });

});

/*******************************************
 * 配置新爬蟲視窗表單動作
/********************************************/
function create_callback(){
    // step1 類型切換之後才需要篩選
    $('.form-horizontal #type').on({
        change : function() {
            val = $(this).val();
            if( val == 'pttbbs' ){
                $(".forptt").removeClass('hide');
            }else{
                $(".forptt").addClass('hide');
            }
        }
    });

}

/*******************************************
 * 預設編輯視窗/刪除按鈕觸發動作
/********************************************/
$(document).on("click", ".btn-modal",function(e){
    $url = $(this).data('remote');
    $exclass = $(this).data('mclass');
    var $callback = window[$(this).data('callback')];

    $.ajax({
        url : $url,
        dataType : 'html',
        beforeSend : function(){
            $("#layoutModal").empty();
        },
        success : function( resp ){
            $("#layoutModal").html( resp ).find('.modal-dialog').addClass($exclass);
        },
        complete : function(){
            $("#layoutModal").modal('show');
            if (typeof $callback === "function" ) {
                $callback();
            }
        }
    });
}).on("click", ".btn-delete",function(e){
    window.location = $(this).data('remote');
});