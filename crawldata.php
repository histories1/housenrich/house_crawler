<?php

class crawldata
{
    /**
     * 目前處理類型「PTT|FB」
     */
    private $typeLabel;

    /**
     * 目前處理類型代碼
     */
    private $typeArr = array('FB'=>0,'PTT'=>1);


    private $jsonfiles = array();


    public function __construct($t, $cid){
        $this->typeLabel = strtoupper($t);
        $this->type = $this->typeArr[$this->typeLabel];

        $this->parserfile($cid);
    }


    /**
     * 處理將取得的資料寫入DB後刪除檔案
     *（之後可以直接透過Python同步寫入json與db！）
     */
    protected function parserfile($cid)
    {
        // json filepath
        $path = realpath(__DIR__).DIRECTORY_SEPARATOR.'public/temp/'.$this->typeLabel;

        // 指定特定群組
        if( $cid != 'all' ){
            $targettabs = \Pkrobot\Models\CategoryTabs::findByCategoryId($cid);
        }else{
            $targettabs = \Pkrobot\Models\Tabs::findByType( $this->type );
        }

        foreach ( $targettabs as $obj) {
            if( $cid != 'all' ){
                $dir = $obj->reltabs->dirname;
                $index = $obj->reltabs->tabsId;
            }else{
                $dir = $obj->dirname;
                $index = $obj->tabsId;
            }

            if( !is_dir($path.DIRECTORY_SEPARATOR.$dir) ){
                continue;
            }

            // read dir
            if ($handle = opendir( $path.DIRECTORY_SEPARATOR.$dir ) ) {
                while (false !== ($entry = readdir($handle))) {
                    // 排除已匯入紀錄！
                    if( !preg_match("/^(\.|imported)/",$entry) && !preg_match("/html$/",$entry) ){
                        $file = $path.DIRECTORY_SEPARATOR.$dir . DIRECTORY_SEPARATOR . $entry;
                        $this->jsonfiles[$index][] = array(
                                                        'time' => str_replace(".json","",$entry),
                                                        'filename' => $entry,
                                                        'fullpath' => $path.DIRECTORY_SEPARATOR.$dir . DIRECTORY_SEPARATOR . $entry
                                                     );
                    }
                }
                closedir($handle);
            }
        }
    }

    /**
     * 檢查主要數據資料是否已存在
     * @param Array 單筆爬蟲紀錄
     */
    protected function nonexist_crawldata( $record )
    {
        if( empty($record['i_連結']) ){
            $row = \Pkrobot\Models\Crawldata::findByTitle( $record['c_標題'] );
        }else{
            $row = \Pkrobot\Models\Crawldata::findBySourcelink( $record['i_連結'] );
        }

        if( count($row->toArray()) == 0  ){
            return true;
        }else{
            if( !$row->getFirst()->tabses->getFirst() ){
                var_dump($row->toArray());die();
            }

            if( $row->getFirst()->tabses->getFirst()->type == $this->type){
                return false;
            }else{
                return true;
            }
        }
    }


    /**
     * 判斷取出內容若為空且無外部連結可能是影片
     */
    protected function FB_external_check( &$value )
    {
        if( $value['f_內文'] == null && $value['i_連結'] == null && $value['c_標題'] == '無原始標題資訊' ){
            $value['c_標題'] = '此篇可能是嵌入影片檔。';
            $value['f_內文'] = '此篇內容推測為嵌入影片資訊，尚未針對此類資訊解析。';
            $value['shareimage'] = array('src'=>'http://placehold.it/100x100?text=Video' ,'width'=>100,'height'=>100);
        }
        return true;
    }


    /**
     * 寫入圖片資訊
     */
    protected function FB_save_extendinfo($crawldataId, $data)
    {
        $extendinfo = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo->CrawldataId = $crawldataId;
        $extendinfo->key = 'shareimage';
        $extendinfo->value = $data['shareimage'];
        $extendinfo->setTime = time();

        if( !$extendinfo->save() ){
            trigger_error('寫入資料庫紀錄發生錯誤，訊息：'.implode(', ', $extendinfo->getMessages()) );
            exit(0);
        }

        $extendinfo = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo->CrawldataId = $crawldataId;
        $extendinfo->key = 'type';
        $extendinfo->value = $data['posttype'];
        $extendinfo->setTime = time();

        if( !$extendinfo->save() ){
            trigger_error('寫入資料庫紀錄發生錯誤，訊息：'.implode(', ', $extendinfo->getMessages()) );
            exit(0);
        }

        return true;
    }


    /**
     * 處理PTT若文章累積推文量未達30則不寫入！
     */
    protected function PTT_external_check( &$value )
    {
        if( (intval($value['h_推文總數']['g'])-intval($value['h_推文總數']['b'])) < 30 ){
            return false;
        }
        return true;
    }


    /**
     * 寫入內文資訊
     */
    protected function PTT_save_extendinfo($crawldataId, $data)
    {
        $extendinfo[0] = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo[0]->CrawldataId = $crawldataId;
        $extendinfo[0]->key = '總數';
        $extendinfo[0]->value = $data['h_推文總數']['all'];

        $extendinfo[1] = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo[1]->CrawldataId = $crawldataId;
        $extendinfo[1]->key = '一般';
        $extendinfo[1]->value = $data['h_推文總數']['n'];

        $extendinfo[2] = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo[2]->CrawldataId = $crawldataId;
        $extendinfo[2]->key = '推文';
        $extendinfo[2]->value = $data['h_推文總數']['g'];

        $extendinfo[3] = new \Pkrobot\Models\CrawldataExtendinfo();
        $extendinfo[3]->CrawldataId = $crawldataId;
        $extendinfo[3]->key = '噓文';
        $extendinfo[3]->value = $data['h_推文總數']['b'];

        foreach ($extendinfo as $info) {
            if( !$info->save() ){
                trigger_error('寫入資料庫紀錄發生錯誤，訊息：'.implode(', ', $info->getMessages()) );
                exit(0);
            }
        }
        return true;
    }


    /**
     * 處理將資料逐筆寫入資料庫
     */
    public function save()
    {
        // 不需處理！
        if( count($this->jsonfiles) == 0 ){
            return false;
        }

        foreach( $this->jsonfiles as $tabId => $files) {
            foreach( $files as $i => $info) {
                // $times=explode("-",$info['time']);
                // $times[3]=substr($times[2],0,2);
                // $times[4]=substr($times[2],2,2);
                if( mb_strlen(file_get_contents($info['fullpath'])) > 10 ){
                foreach(json_decode(file_get_contents($info['fullpath']), true) as $value) {
                    $tab = \Pkrobot\Models\Tabs::findFirst($tabId);
                    $tab->refreshTime = strtotime($info['time']);
                    $tab->save();
                    // $value['refreshTime'] = "{$times[0]}月{$times[1]}日 {$times[3]}:{$times[4]}";
                    // 判斷是否已存在相同紀錄
                    if( $this->nonexist_crawldata( $value ) ){
                        // act1. insert to Crawldata
                        $crawldata = new \Pkrobot\Models\Crawldata();

                        // FB檢查是否為影片、PTT檢查一般加推文總數超過30
                        if( !$this->{$this->typeLabel."_external_check"}( $value ) ){
                            continue;
                        }

                        $crawldata->title = $value['c_標題'];
                        $crawldata->content = (empty($value['f_內文'])) ? '&nbsp;' : $value['f_內文'];
                        $crawldata->initTime = $value['d_時間戳記'];
                        $crawldata->sourcelink = $value['i_連結'];
                        $tc = new \Pkrobot\Models\TabsCrawldata();
                        $tc->TabsId = $tabId;
                        // 改用dirname作為關聯欄位
                        $tc->Dirname = $tab->dirname;
                        $crawldata->reltabcrawldata = $tc;
                        // act2. insert to TabCrawldata
                        if( !$crawldata->save() ){
                            var_dump($crawldata->toArray());
                            trigger_error('寫入資料庫紀錄發生錯誤，訊息：'.implode(', ', $crawldata->getMessages()) );
                            exit(0);
                        }

                        $this->{$this->typeLabel."_save_extendinfo"}( $crawldata->crawldataId , $value );
                    }
                }
                }

                // 刪除完成處理資料檔案 // 完成紀錄寫入後，將該筆json重新命名imported-xxx
                $this->delete( $info );
            }
        }

        return true;
    }


    /**
     * 刪除或變更處理檔名作法
     */
    protected function delete($fi, $mod=true)
    {
        // 直接刪除
        if( file_exists($fi['fullpath']) && $mod ){
            if( $mod ){
                @unlink($fi['fullpath']);
            }else{
                $o = str_replace('-','\-',$fi['filename']);
                $n = 'imported-'.$fi['filename'];
                rename($fi['fullpath'], preg_replace("/{$o}/", $n, $fi['fullpath']));
            }
        }
    }

}