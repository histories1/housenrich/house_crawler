<?php

/*
 * Copyright 2014 Michał Strzelczyk
 * mail: kontakt@michalstrzelczyk.pl
 *
 */
namespace Pkrobot\Forms;

use Personalwork\Forms\DecoratorAbstract as DecoratorAbstract;
use Phalcon\Tag as Tag;

class DecoratorCheckbox extends DecoratorAbstract{

    /**
     * Generate element html
     *
     * @return void
     */
    public function generateElement()
    {
        $this->html.= "\t\t\t".Tag::tagHtml('div',array('class'=>'col-sm-9'), FALSE, TRUE, TRUE);
        $this->html.= "\t\t\t".$this->element->render().PHP_EOL;

        $this->generateHelpBlock();
        $this->generateErrors();

        $this->html.= Tag::tagHtmlClose('div').PHP_EOL;
    }

    public function toHtml() {

        $this->html.=  "\t".Tag::tagHtml('div', array('class'=>'form-group'), FALSE, TRUE, TRUE).PHP_EOL;

        $this->generateLabel();
        $this->generateElement();

        $this->html.=  "\t".Tag::tagHtmlClose('div').PHP_EOL;;

        return $this->html;
    }

}
