<?php

namespace Crawler\Models;

class Tabs extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $tabsId;

    /**
     * @Comment("類型(指定英文代碼)")
     *
     * @var string
     */
    public $type;

    /**
     * @Comment("頁籤名稱")
     *
     * @var string
     */
    public $label;

    /**
     * @Comment("資料來源(url or board-alias)")
     *
     * @var string
     */
    public $source;

    /**
     * @Comment("設定爬蟲所屬操作者編號")
     *
     * @var integer
     */
    public $PeopleId;

    /**
     * @Comment("擷取網址結尾")
     *
     * @var string
     */
    public $endstring;

    /**
     * @Comment("Python抓取暫存資料夾")
     *
     * @var string
     */
    public $dirname;

    /**
     * @Comment("時間範圍(單位:分鐘)")
     *
     * @var integer
     */
    public $crontime;

    /**
     * @Comment("時間內抓次數")
     *
     * @var integer
     */
    public $crawllimit;

    /**
     * 591類網站 /
     * 標題、圖片(外部網址)、坪單價、總價、地址到地段
     * */
    const TYPE_1 = 0;

    /**
     * PTT
     * */
    const TYPE_PTT = 1;

    public $types = array(null, '591' => '591網頁', 'sinyi' => '信義房屋');

    public function getTypes() {
        return $this->types;
    }

    public function getTypeLabel() {
        return $this->types[$this->type];
    }

    public function initialize()
    {
        $this->hasManyToMany(
            "tabsId",
            "Crawler\Models\TabsCrawldata",
            "TabsId", "CrawldataId",
            "Crawler\Models\Crawldata",
            "crawldataId",
            array('alias' => 'crawldatas')
        );

        $this->hasMany('tabsId', 'Crawler\Models\CategoryTabs', 'TabsId', array('alias' => 'CategoryTabs'));
        $this->hasMany("tabsId", "Crawler\Models\TabsState", "TabsId", array('alias' => 'states'));
        $this->belongsTo("PeopleId", "Crawler\Models\People", "peopleId", array('alias' => 'relpeople'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'tabs';
    }


    public function setDirname($name=null) {
        if( empty($name) ){
            $pinyin = new \Personalwork\Package\Pinyin();
            $this->dirname = $pinyin->converter($this->label);
        }else{
            $this->dirname = $name;
        }
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tabs[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Tabs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
