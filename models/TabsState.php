<?php
/**
 * 爬蟲機器人更新數據狀態紀錄表
 */

namespace Crawler\Models;

class TabsState extends \Phalcon\Mvc\Model
{

    /**
     * 主鍵
     * @var integer
     */
    public $tstateId;

    /**
     * 關聯爬蟲機器編號
     * @var integer
     */
    public $TabsId;

    /**
     * 關聯使用者帳號
     * @var integer
     */
    public $PeopleId;

    /**
     * 擷取數據狀態
     * @var integer
     */
    public $statecode;

    /**
     * 更新紀錄時間
     * @var integer
     */
    public $setTime;

    /**
     * 系統擷取備註資訊
     * @var string
     */
    public $comments;


    const STAT_INIT = 0;

    const STAT_UPDATE = 1;
    /**
     * set at self::beforeValidationOnCreate()
     */
    const STAT_OLDRECORD = 3;


    const STAT_REMOVE = 4;

    public $stateLabels = array('新增','更新','保存紀錄', '移除');


    public function initialize()
    {
        $this->belongsTo("TabsId", "Crawler\Models\Tabs", "tabsId", array('alias' => 'reltabs'));
        $this->belongsTo("PeopleId", "Crawler\Models\People", "peopleId", array('alias' => 'relpeople'));
    }


    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'tstateId' => 'tstateId',
            'TabsId' => 'TabsId',
            'PeopleId' => 'PeopleId',
            'statecode' => 'statecode',
            'setTime' => 'setTime',
            'comments' => 'comments'
        );
    }

}
