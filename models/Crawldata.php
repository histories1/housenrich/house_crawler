<?php

namespace Crawler\Models;

class Crawldata extends \Phalcon\Mvc\Model
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $crawldataId;

    /**
     * @Comment("標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("存取來源")
     *
     * @var string
     */
    public $sourcelink;

    /**
     * @Comment("封面圖")
     *
     * @var string
     */
    public $cover;

    /**
     * @Comment("縣市")
     *
     * @var string
     */
    public $addressCity;

    /**
     * @Comment("行政區")
     *
     * @var string
     */
    public $addressDistrict;

    /**
     * @Comment("原始抓取地址")
     *
     * @var string
     */
    public $addressSegment;

    /**
     * @Comment("顯示地址")
     *
     * @var string
     */
    public $addressShow;

    /**
     * @Comment("總價")
     *
     * @var double
     */
    public $priceTotal;

    /**
     * @Comment("降價總價")
     *
     * @var double
     */
    public $priceDown;

    /**
     * @Comment("類型")
     *
     * @var string
     */
    public $type;

    /**
     * @Comment("用途")
     *
     * @var string
     */
    public $usefor;

    /**
     * @Comment("房數")
     *
     * @var integer
     */
    public $patternRoom;

    /**
     * @Comment("廳數")
     *
     * @var integer
     */
    public $patternHall;

    /**
     * @Comment("衛浴數")
     *
     * @var integer
     */
    public $patternBathroom;

    /**
     * @Comment("陽台數")
     *
     * @var integer
     */
    public $patternBalcony;

    /**
     * @Comment("所在樓層")
     *
     * @var integer
     */
    public $floorAt;

    /**
     * @Comment("總樓層")
     *
     * @var integer
     */
    public $floorTotal;

    /**
     * @Comment("屋齡")
     *
     * @var double
     */
    public $houseage;

    /**
     * @Comment("有無車位")
     *
     * @var integer
     */
    public $parking;

    /**
     * @Comment("總坪數")
     *
     * @var double
     */
    public $areaTotal;

    /**
     * @Comment("不含車位坪數")
     *
     * @var double
     */
    public $areaMain;

    /**
     * @Comment("車位坪數")
     *
     * @var double
     */
    public $areaParking;

    /**
     * @Comment("緯度")
     *
     * @var double
     */
    public $lat;

    /**
     * @Comment("經度")
     *
     * @var double
     */
    public $lng;

    /**
     * @Comment("地圖網址")
     *
     * @var string
     */
    public $sourceMap;

    /**
     * @Comment("社區")
     *
     * @var string
     */
    public $community;

    /**
     * @Comment("物件抓取時間")
     *
     * @var integer
     */
    public $initTime;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('crawldataId', 'CrawldataExtendinfo', 'CrawldataId', array('alias' => 'CrawldataExtendinfo'));
        $this->hasMany('crawldataId', 'CrawldataState', 'CrawldataId', array('alias' => 'CrawldataState'));
        $this->hasMany('crawldataId', 'TabsCrawldata', 'CrawldataId', array('alias' => 'TabsCrawldata'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Crawldata[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Crawldata
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'crawldata';
    }

}
