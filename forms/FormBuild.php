<?php

namespace Pkrobot\Forms;

use Personalwork\Forms\FormGroups;

class FormBuild extends FormGroups
{
    public $options = array(
                        'target_dir' => '',
                        //目標檔案內設定參數： "belongto" : "表單id" 即符合群組配置
                        'belongto'  => '',
                        //設定表單送出按鈕名稱
                        'sendbtnname' => '設定'
                      );

    /**
     * Phalcon\Forms\Form預設第一個參數搭配表單對應ORM class
     */
    public function initialize($entity, $opt)
    {
        //取得表單配置設定檔
        $this->options = $opt;

        $this   ->setAction('post')
                ->setEnctype('multipart/form-data')
                ->setMethod('post')
                ->setCssClass('form-horizontal')
                ->setCssId( $this->options['belongto'] );

        //add all another elements to form
        if( is_dir($this->options['target_dir']) && is_readable($this->options['target_dir']) ){
            //load conf file for configure form build
            if ($handle = opendir( $this->options['target_dir']) ) {
                while (false !== ($entry = readdir($handle))) {
                    if( !preg_match("/^\./",$entry) ){
                        $conf[] = $this->options['target_dir'] . $entry;
                    }
                }
                closedir($handle);
            }
        }

        if( isset($conf) && count($conf) > 0){
        foreach($conf as $cfile){
            //act1. 配置JSON設定檔格式
            $configs[] = new \Phalcon\Config\Adapter\Json( $cfile );
        }
        }

        if( isset($configs) && count($configs) > 0){
        foreach( $configs as $set ){
            if( empty($set->belongto) or $set->belongto != $this->options['belongto'] ){
                continue ;
            }
            $this->addFieldSet($set);
        }
        }

        $this->add(new \Phalcon\Forms\Element\Submit('send', array('class' => 'btn btn-success', 'value'=>$this->options['sendbtnname'])));
    }


    /**
     * Render all Form
     */
    public function toHtml()
    {
         $helper = new ViewHelper( $this );
         return $helper->toHtml('widget');
    }
}

?>