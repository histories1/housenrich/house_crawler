<?php
/**
 * 爬蟲機器紀錄延伸資訊表
 */

namespace Crawler\Models;

class CrawldataExtendinfo extends \Phalcon\Mvc\Model
{

    /**
     * 設定同物件不同網址或不同來源網站個別資訊
     * 1. 物件編號
     * 2. 連結網址
     * 3. 總價
     * 使用serialize()處理
     * */
    const KAY_SOURCE_DATASET = '來源標籤';

    /**
     * 主鍵
     * @var integer
     */
    public $cextendinfoId;

    /**
     * 關聯爬蟲紀錄編號
     * @var integer
     */
    public $CrawldataId;

    /**
     * 鍵值
     * @var string
     */
    public $key;

    /**
     * 數值
     * @var string
     */
    public $value;

    /**
     * 記錄時間
     * @var integer
     */
    public $setTime;

    public function initialize()
    {
        $this->belongsTo("CrawldataId", "Crawler\Models\Crawldata", "crawldataId", array('alias' => 'relcrawldata'));

        //在使用update時排除特定欄位
        // $this->skipAttributesOnUpdate(array('setTime'));
    }


    public function beforeValidationOnCreate()
    {
        $this->setTime = time();
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cextendinfoId' => 'cextendinfoId',
            'CrawldataId' => 'CrawldataId',
            'key' => 'key',
            'value' => 'value',
            'setTime' => 'setTime'
        );
    }

}
