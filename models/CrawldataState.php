<?php
/**
 * 爬蟲機器紀錄資訊狀態表
 */

namespace Crawler\Models;

class CrawldataState extends \Phalcon\Mvc\Model
{

    /**
     * 主鍵
     * @var integer
     */
    public $cstateId;

    /**
     * 關聯爬蟲機器編號
     * @var integer
     */
    public $CrawldataId;

    /**
     * 紀錄狀態碼
     * @var integer
     */
    public $statecode;

    /**
     * 記錄時間
     * @var integer
     */
    public $setTime;

    const STAT_INIT = 0;

    const STAT_UPDATE = 1;

    const STAT_OLDRECORD = 3;

    const STAT_REMOVE = 4;

    public $stateLabels = array('新增','更新','保存紀錄', '移除');


    public function initialize()
    {
        $this->belongsTo("CrawldataId", "Crawler\Models\Crawldata", "crawldataId", array('alias' => 'relcrawldata'));
    }


    public function beforeValidationOnCreate()
    {
        $this->setTime = time();
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cstateId' => 'cstateId',
            'CrawldataId' => 'CrawldataId',
            'statecode' => 'statecode',
            'setTime' => 'setTime'
        );
    }

}
