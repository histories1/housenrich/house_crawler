<?php
/**
 * cli $phalcon model --name=people --namespace=Personalwork\\Mvc\\Model\\Basic --get-set --doc --force people
 */

namespace Crawler\Models;

class People extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $peopleId;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $account;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $lastTime;


    public function initialize()
    {
        $this->hasMany("peopleId", "Crawler\Models\Tabs", "PeopleId", array('alias' => 'tabs'));
        $this->hasMany("peopleId", "Crawler\Models\Category", "PeopleId", array('alias' => 'categorys'));

        $this->skipAttributesOnCreate(array('lastTime'));
    }


    public function beforeValidationOnUpdate()
    {
        $this->lastTime = time();
    }

    public function afterFetch()
    {
        if( !empty($this->lastTime) )
            $this->lastTime = date('Y年m月d日 H:i:s',$this->lastTime);
    }
}