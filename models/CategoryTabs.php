<?php
/**
 * 爬蟲機器紀錄資訊表
 */

namespace Crawler\Models;

class CategoryTabs extends \Phalcon\Mvc\Model
{

    /**
     * 主鍵
     * @var integer
     */
    public $ctId;

    /**
     * 關聯爬蟲機器編號
     * @var integer
     */
    public $TabsId;

    /**
     * 關聯分類編號
     * @var integer
     */
    public $CategoryId;


    public function initialize()
    {
        $this->belongsTo("TabsId", "Crawler\Models\Tabs", "tabsId", array('alias' => 'reltabs'));
        $this->belongsTo("CategoryId", "Crawler\Models\Category", "categoryId", array('alias' => 'relcategory'));
    }


    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'ctId' => 'ctId',
            'TabsId' => 'TabsId',
            'CategoryId' => 'CategoryId'
        );
    }

}
